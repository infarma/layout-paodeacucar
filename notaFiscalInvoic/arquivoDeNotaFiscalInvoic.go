package notaFiscalInvoic

import (
	"fmt"
	"regexp"
	"strings"
	"time"
)

type ArquivoDeNotaFiscalInvoic struct {
	RegistroHeader1              RegistroHeader1              `json:"RegistroHeader1"`
	RegistroCondicoesDePagamento RegistroCondicoesDePagamento `json:"RegistroCondicoesDePagamento"`
	RegistroHeader3              RegistroHeader3              `json:"RegistroHeader3"`
	RegistroDetalhe              []RegistroDetalhe            `json:"RegistroDetalhe"`
	RegistroTrailer              RegistroTrailer              `json:"RegistroTrailer"`
}

// registro 1 heander1

func Header1Nota( FuncaoMsg string, NumeroNf int64, CodSN int64, DataHoraEN time.Time, DataHoraSN time.Time, DataHoraPE time.Time, CodFiscalOp int64, NumeroPed1 int64, NumeroPed2 int64, NumeroPed3 int64, CodEanComp int64, CodEanEm int64, NumCgcEm int64, NumInsEst string, CodUf string, CodEanLE string, CodEanLC string, CodBan int64, CodAgen int64, NumContaCorr int64) string {

	Header1 := fmt.Sprint("01")
	Header1 += fmt.Sprintf("%01s", FuncaoMsg)
	Header1 += fmt.Sprintf("%09d", NumeroNf)
	Header1 += fmt.Sprintf("%03d", CodSN)

	str1 := DataHoraEN.String()
	str2 := DataHoraSN.String()
	str3 := DataHoraPE.String()
	x := str1[0:16]
	y := str2[0:16]
	z := str3[0:16]
	re1 := regexp.MustCompile(`:|-|\s`)
	re2 := regexp.MustCompile(`:|-|\s`)
	re3 := regexp.MustCompile(`:|-|\s`)
	data1 := re1.ReplaceAllLiteralString(x, "")
	data2 := re2.ReplaceAllLiteralString(y, "")
	data3 := re3.ReplaceAllLiteralString(z, "")
	Header1 += fmt.Sprintf("%012s", data1)
	Header1 += fmt.Sprintf("%012s", data2)
	Header1 += fmt.Sprintf("%012s", data3)

	Header1 += fmt.Sprintf("%04d", CodFiscalOp)
	Header1 += fmt.Sprintf("%015d", NumeroPed1)
	Header1 += fmt.Sprintf("%015d", NumeroPed2)
	Header1 += fmt.Sprintf("%015d", NumeroPed3)
	Header1 += fmt.Sprintf("%013d", CodEanComp)
	Header1 += fmt.Sprintf("%013d", CodEanEm)
	Header1 += fmt.Sprintf("%015d", NumCgcEm)
	Header1 += fmt.Sprintf("%020s", NumInsEst)
	Header1 += fmt.Sprintf("%02s", CodUf)
	Header1 += fmt.Sprintf("%013s", CodEanLE)
	Header1 += fmt.Sprintf("%013s", CodEanLC)
	Header1 += fmt.Sprintf("%04d", CodBan)
	Header1 += fmt.Sprintf("%05d", CodAgen)
	Header1 += fmt.Sprintf("%011d", NumContaCorr)
	Header1 += fmt.Sprintf("                                                                       ")

	return Header1
}

// Registro Tipo 2 - Condições de Pagamento
func CondicoesDePagamentoNota(NumeroNF int64, CodSN int64, TipoDePag string, ReferenciaDePr int64, DescricaoCP string, QtdeDiasCP int64, DataDeVenc time.Time, PorcentagemDF float64, PorcentagemAPF float64) string {

	Condicoes := fmt.Sprint("02")
	Condicoes += fmt.Sprintf("%09d", NumeroNF)
	Condicoes += fmt.Sprintf("%03d", CodSN)
	Condicoes += fmt.Sprintf("%-3s", TipoDePag)
	Condicoes += fmt.Sprintf("%01d", ReferenciaDePr)
	Condicoes += fmt.Sprintf("%02s", DescricaoCP)
	Condicoes += fmt.Sprintf("%03d", QtdeDiasCP)
	str := DataDeVenc.String()
	x := str[0:10]
	re := regexp.MustCompile(`:|-|\s`)
	data := re.ReplaceAllLiteralString(x, "")
	Condicoes += fmt.Sprintf("%08s", data)
	Condicoes += fmt.Sprintf("%06s", strings.Replace(fmt.Sprintf("%.3f", PorcentagemDF), ".", "", 1))
	Condicoes += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PorcentagemAPF), ".", "", 1))
	Condicoes += fmt.Sprintf("                                                                                                                                                                                                                                              ")

	return Condicoes
}

// Registro Tipo 3 - Header3

func Header3Nota(  NumeroNf int64, CodSN int64, TipoVeic string, CodEan int64, TipoIden string, NomeTr string,
	IdentPV string , TipoF string, ValorEF float64, TaxaAlFrete float64, TaxaAlSeguro float64) string {

	Header3 := fmt.Sprint("03")
	Header3 += fmt.Sprintf("%09d", NumeroNf)
	Header3 += fmt.Sprintf("%03d", CodSN)
	Header3 += fmt.Sprintf("%04s", TipoVeic)
	Header3 += fmt.Sprintf("%015d", CodEan)
	Header3 += fmt.Sprintf("%03s", TipoIden)
	Header3 += fmt.Sprintf("%025s", NomeTr)
	Header3 += fmt.Sprintf("%012s", IdentPV)
	Header3 += fmt.Sprintf("%03s", TipoF)
	Header3 += fmt.Sprintf("%013s", strings.Replace(fmt.Sprintf("%.2f", ValorEF), ".", "", 1))
	Header3 += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", TaxaAlFrete), ".", "", 1))
	Header3 += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", TaxaAlSeguro), ".", "", 1))
	Header3 += fmt.Sprintf("                                                                                                                                                                                     ")

	return Header3
}


// Registro tipo 4 - Detalhe
func DetalheNota(NumeroNotaFiscal int64, CodSerieNota int64, CodEanProduto int64, VolumeTotal float64, QtdeFaturada float64, UnidadeMedidaFaturada int64, QtdeEntrega float64, UnidadeMedidaEntregue string, PesoTotalItemEntrega float64, ValorBrutoTotal float64, ValorLiquidoTotal float64, ValorBrutoUnitario float64, ValorLiquidoUnitario float64, CodSituacaoTributaria string, TaxaAliquotaIcms float64, TaxaAliquotaIcsmSubstituicaoTributaria float64, TaxaAliquotaIpi float64, TaxaPercentualDescontoComercial float64, ValorDescontoComercial float64, PorcentagemReducaoBaseIcms float64, ValorDeBaseDeCalculoDeIcms float64, ValorDoIcmsCalculado float64 , ValorDeBaseDeCalculoDeIcmsSt float64, ValorDoIcmsClaculadoSt float64	, CodigoNCM int64, CodigoNCMEX int64, CodigoCSTPIS int64, CodigoCSTCOFINS int64, CodigoCSTIPI int64, CodigoCSTICMS int64, TaxaAliquotaPIS float64, TaxaAliquotaCOFINS float64, TaxaMvaIcmsST float64) string {

	Detalhe := fmt.Sprint("04")
	Detalhe += fmt.Sprintf("%09d", NumeroNotaFiscal)
	Detalhe += fmt.Sprintf("%03d", CodSerieNota)
	Detalhe += fmt.Sprintf("%014d", CodEanProduto)
	Detalhe += fmt.Sprintf("%010s", strings.Replace(fmt.Sprintf("%.3f", VolumeTotal), ".", "", 1))
	Detalhe += fmt.Sprintf("%010s", strings.Replace(fmt.Sprintf("%.3f", QtdeFaturada), ".", "", 1))
	Detalhe += fmt.Sprintf("%03d", UnidadeMedidaFaturada)
	Detalhe += fmt.Sprintf("%010s", strings.Replace(fmt.Sprintf("%.3f", QtdeEntrega), ".", "", 1))
	Detalhe += fmt.Sprintf("%03s", UnidadeMedidaEntregue)
	Detalhe += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.3f", PesoTotalItemEntrega), ".", "", 1))
	Detalhe += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f", ValorBrutoTotal), ".", "", 1))
	Detalhe += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f", ValorLiquidoTotal), ".", "", 1))
	Detalhe += fmt.Sprintf("%013s", strings.Replace(fmt.Sprintf("%.4f", ValorBrutoUnitario), ".", "", 1))
	Detalhe += fmt.Sprintf("%013s", strings.Replace(fmt.Sprintf("%.4f", ValorLiquidoUnitario), ".", "", 1))
	Detalhe += fmt.Sprintf("%02s", CodSituacaoTributaria)
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", TaxaAliquotaIcms), ".", "", 1))
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", TaxaAliquotaIcsmSubstituicaoTributaria), ".", "", 1))
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", TaxaAliquotaIpi), ".", "", 1))
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", TaxaPercentualDescontoComercial), ".", "", 1))
	Detalhe += fmt.Sprintf("%013s", strings.Replace(fmt.Sprintf("%.2f", ValorDescontoComercial), ".", "", 1))
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PorcentagemReducaoBaseIcms), ".", "", 1))
	Detalhe += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f", ValorDeBaseDeCalculoDeIcms), ".", "", 1))
	Detalhe += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f", ValorDoIcmsCalculado), ".", "", 1))
	Detalhe += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f", ValorDeBaseDeCalculoDeIcmsSt), ".", "", 1))
	Detalhe += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f", ValorDoIcmsClaculadoSt), ".", "", 1))
	Detalhe += fmt.Sprintf("%08d", CodigoNCM)
	Detalhe += fmt.Sprintf("%02d", CodigoNCMEX)
	Detalhe += fmt.Sprintf("%02d", CodigoCSTPIS)
	Detalhe += fmt.Sprintf("%02d", CodigoCSTCOFINS)
	Detalhe += fmt.Sprintf("%02d", CodigoCSTIPI)
	Detalhe += fmt.Sprintf("%02d", CodigoCSTICMS)
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", TaxaAliquotaPIS), ".", "", 1))
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", TaxaAliquotaCOFINS), ".", "", 1))
	Detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", TaxaMvaIcmsST), ".", "", 1))
	Detalhe += fmt.Sprintf("                   ")

	return Detalhe
}

// Registro tipo 9 - Trailer

func TrailerNota( NumeroNotaFiscal int64, CodSerieNota int64, NumTotalItensNota int64, NumTotalEmbalagens int64, QtdeTotalPallets float64, TotalPesoBruto float64, TotalPesoLiquido float64, ValorBaseDeCalculoIcms float64, ValorTotalIcms float64, ValorBaseCalculoIcmsSubstituicao float64, ValorTotalIcmsSubstituicao float64, ValorTotalMercadorias float64, ValorTotalFrete float64, ValorTotalSeguro float64, ValorTotalDespAcessoriaTributada float64, ValorBaseCalculoIpi float64, ValorTotalIpi float64, ValorTotalDescontos float64, ValorTotalNota float64, ValotTotalDespAcessoriaNaoTributada float64) string {

	Trailer := fmt.Sprint("09")
	Trailer += fmt.Sprintf("%09d", NumeroNotaFiscal)
	Trailer += fmt.Sprintf("%03d", CodSerieNota)
	Trailer += fmt.Sprintf("%04d", NumTotalItensNota)
	Trailer += fmt.Sprintf("%04d", NumTotalEmbalagens)
	Trailer += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f", QtdeTotalPallets), ".", "", 1))
	Trailer += fmt.Sprintf("%09s", strings.Replace(fmt.Sprintf("%.3f", TotalPesoBruto), ".", "", 1))
	Trailer += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.3f",  TotalPesoLiquido), ".", "", 1))
	Trailer += fmt.Sprintf("%016s", strings.Replace(fmt.Sprintf("%.2f", ValorBaseDeCalculoIcms), ".", "", 1))
	Trailer += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f",  ValorTotalIcms), ".", "", 1))
	Trailer += fmt.Sprintf("%016s", strings.Replace(fmt.Sprintf("%.2f",  ValorBaseCalculoIcmsSubstituicao), ".", "", 1))
	Trailer += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f",  ValorTotalIcmsSubstituicao), ".", "", 1))
	Trailer += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f",  ValorTotalMercadorias), ".", "", 1))
	Trailer += fmt.Sprintf("%013s", strings.Replace(fmt.Sprintf("%.2f",  ValorTotalFrete), ".", "", 1))
	Trailer += fmt.Sprintf("%013s", strings.Replace(fmt.Sprintf("%.2f",  ValorTotalSeguro), ".", "", 1))
	Trailer += fmt.Sprintf("%013s", strings.Replace(fmt.Sprintf("%.2f",  ValorTotalDespAcessoriaTributada), ".", "", 1))
	Trailer += fmt.Sprintf("%016s", strings.Replace(fmt.Sprintf("%.2f",  ValorBaseCalculoIpi), ".", "", 1))
	Trailer += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f",  ValorTotalIpi), ".", "", 1))
	Trailer += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f",  ValorTotalDescontos), ".", "", 1))
	Trailer += fmt.Sprintf("%015s", strings.Replace(fmt.Sprintf("%.2f",  ValorTotalNota), ".", "", 1))
	Trailer += fmt.Sprintf("%013s", strings.Replace(fmt.Sprintf("%.2f",  ValotTotalDespAcessoriaNaoTributada), ".", "", 1))
	Trailer += fmt.Sprintf("                                    ")

	return Trailer
}




