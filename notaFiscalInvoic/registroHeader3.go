package notaFiscalInvoic

type RegistroHeader3 struct {
	CodRegistro               int64  	`json:"CodRegistro"`
	NumeroNotaFiscal          int64  	`json:"NumeroNotaFiscal"`
	CodSerieNota              int64  	`json:"CodSerieNota"`
	TipoVeiculo               string 	`json:"TipoVeiculo"`
	CodEanOuCgcTransportadora int64  	`json:"CodEanOuCgcTransportadora"`
	TipoIdentificacaoTransp   string 	`json:"TipoIdentificacaoTransp"`
	NomeTransportadora        string 	`json:"NomeTransportadora"`
	IdentPlacaVeiculo         string 	`json:"IdentPlacaVeiculo"`
	TipoFrete                 string 	`json:"TipoFrete"`
	ValorEncargosFinanceiros  float64	`json:"ValorEncargosFinanceiros"`
	TaxaAliquotaIcmsFrete     float64	`json:"TaxaAliquotaIcmsFrete"`
	TaxaAliquotaIcmsSeguro    float64	`json:"TaxaAliquotaIcmsSeguro"`
	Filler                    string 	`json:"Filler"`
}
