package notaFiscalInvoic

type RegistroDetalhe struct {
	CodRegistro                            int64  	`json:"CodRegistro"`
	NumeroNotaFiscal                       int64  	`json:"NumeroNotaFiscal"`
	CodSerieNota                           int64  	`json:"CodSerieNota"`
	CodEanProduto                          int64  	`json:"CodEanProduto"`
	VolumeTotal                            float64	`json:"VolumeTotal"`
	QtdeFaturada                           float64	`json:"QtdeFaturada"`
	UnidadeMedidaFaturada                  int64  	`json:"UnidadeMedidaFaturada"`
	QtdeEntrega                            float64	`json:"QtdeEntrega"`
	UnidadeMedidaEntregue                  string 	`json:"UnidadeMedidaEntregue"`
	PesoTotalItemEntrega                   float64	`json:"PesoTotalItemEntrega"`
	ValorBrutoTotal                        float64	`json:"ValorBrutoTotal"`
	ValorLiquidoTotal                      float64	`json:"ValorLiquidoTotal"`
	ValorBrutoUnitario                     float64	`json:"ValorBrutoUnitario"`
	ValorLiquidoUnitario                   float64  	`json:"ValorLiquidoUnitario"`
	CodSituacaoTributaria                  string 	`json:"CodSituacaoTributaria"`
	TaxaAliquotaIcms                       float64	`json:"TaxaAliquotaIcms"`
	TaxaAliquotaIcsmSubstituicaoTributaria float64	`json:"TaxaAliquotaIcsmSubstituicaoTributaria"`
	TaxaAliquotaIpi                        float64	`json:"TaxaAliquotaIpi"`
	TaxaPercentualDescontoComercial        float64	`json:"TaxaPercentualDescontoComercial"`
	ValorDescontoComercial                 float64	`json:"ValorDescontoComercial"`
	PorcentagemReducaoBaseIcms             float64	`json:"PorcentagemReducaoBaseIcms"`
	ValorDeBaseDeCalculoDeIcms             float64	`json:"ValorDeBaseDeCalculoDeIcms"`
	ValorDoIcmsCalculado                   float64	`json:"ValorDoIcmsCalculado"`
	ValorDeBaseDeCalculoDeIcmsSt           float64	`json:"ValorDeBaseDeCalculoDeIcmsSt"`
	ValorDoIcmsClaculadoSt                 float64	`json:"ValorDoIcmsClaculadoSt"`
	CodigoNCM                              int64	`json:"CodigoNCM"`
	CodigoNCMEX                            int64	`json:"CodigoNCMEX"`
	CodigoCSTPIS                           int64	`json:"CodigoCSTPIS"`
	CodigoCSTCOFINS                        int64	`json:"CodigoCSTCOFINS"`
	CodigoCSTIPI                           int64	`json:"CodigoCSTIPI"`
	CodigoCSTICMS                          int64	`json:"CodigoCSTICMS"`
	TaxaAliquotaPIS                        float64	`json:"TaxaAliquotaPIS"`
	TaxaAliquotaCOFINS                     float64	`json:"TaxaAliquotaCOFINS"`
	TaxaMvaIcmsST                          float64	`json:"TaxaMvaIcmsST"`
	Filler                                 string	`json:"Filler"`
}
