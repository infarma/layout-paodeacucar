package notaFiscalInvoic

import "time"

type RegistroHeader1 struct {
	CodRegistro                       int64 	    `json:"CodRegistro"`
	FuncaoMensagem                    string	    `json:"FuncaoMensagem"`
	NumeroNotaFiscal                  int64 	    `json:"NumeroNotaFiscal"`
	CodSerieNota                      int64 	    `json:"CodSerieNota"`
	DataHoraEmissaoNota               time.Time 	`json:"DataHoraEmissaoNota"`
	DataHoraSaidaNota                 time.Time 	`json:"DataHoraSaidaNota"`
	DataHoraPrevisaoEntrega           time.Time 	`json:"DataHoraPrevisaoEntrega"`
	CodFiscalOperacao                 int64 	    `json:"CodFiscalOperacao"`
	NumeroPedido1                     int64 	    `json:"NumeroPedido1"`
	NumeroPedido2                     int64 	    `json:"NumeroPedido2"`
	NumeroPedido3                     int64 	    `json:"NumeroPedido3"`
	CodEanComprador                   int64 	    `json:"CodEanComprador"`
	CodEanEmissorFatura               int64 	    `json:"CodEanEmissorFatura"`
	NumCgcEmissorFatura               int64 	    `json:"NumCgcEmissorFatura"`
	NumInscricaoEstadualEmissorFatura string	    `json:"NumInscricaoEstadualEmissorFatura"`
	CodUfInscrEstEmissorFatura        string	    `json:"CodUfInscrEstEmissorFatura"`
	CodEanLocalEntrega                string	    `json:"CodEanLocalEntrega"`
	CodEanLocalCobranca               string	    `json:"CodEanLocalCobranca"`
	CodBanco                          int64 	    `json:"CodBanco"`
	CodAgenciaBancaria                int64 	    `json:"CodAgenciaBancaria"`
	NumContaCorrente                  int64 	    `json:"NumContaCorrente"`
	Filler                            string	    `json:"Filler"`
}
