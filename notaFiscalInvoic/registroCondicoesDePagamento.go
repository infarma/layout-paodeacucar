package notaFiscalInvoic

import "time"

type RegistroCondicoesDePagamento struct {
	CodRegistro                   int64  	`json:"CodRegistro"`
	NumeroNotaFiscal              int64  	`json:"NumeroNotaFiscal"`
	CodSerieNota                  int64  	`json:"CodSerieNota"`
	TipoDePagamento               string  	`json:"TipoDePagamento"`
	ReferenciaDePrazo             int64  	`json:"ReferenciaDePrazo"`
	DescricaoCondPagto            string 	`json:"DescricaoCondPagto"`
	QtdeDiasCondPagto             int64  	`json:"QtdeDiasCondPagto"`
	DataDeVencimento              time.Time  	`json:"DataDeVencimento"`
	PorcentagemDescontoFinanceiro float64	`json:"PorcentagemDescontoFinanceiro"`
	PorcentagemAPagarFatura       float64	`json:"PorcentagemAPagarFatura"`
	Filler                        string 	`json:"Filler"`
}
