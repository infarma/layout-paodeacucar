package notaFiscalInvoic

type RegistroTrailer struct {
	CodRegistro                         int64  	`json:"CodRegistro"`
	NumeroNotaFiscal                    int64  	`json:"NumeroNotaFiscal"`
	CodSerieNota                        int64  	`json:"CodSerieNota"`
	NumTotalItensNota                   int64  	`json:"NumTotalItensNota"`
	NumTotalEmbalagens                  int64  	`json:"NumTotalEmbalagens"`
	QtdeTotalPallets                    float64  	`json:"QtdeTotalPallets"`
	TotalPesoBruto                      float64  	`json:"TotalPesoBruto"`
	TotalPesoLiquido                    float64	`json:"TotalPesoLiquido"`
	ValorBaseDeCalculoIcms              float64	`json:"ValorBaseDeCalculoIcms"`
	ValorTotalIcms                      float64	`json:"ValorTotalIcms"`
	ValorBaseCalculoIcmsSubstituicao    float64	`json:"ValorBaseCalculoIcmsSubstituicao"`
	ValorTotalIcmsSubstituicao          float64	`json:"ValorTotalIcmsSubstituicao"`
	ValorTotalMercadorias               float64	`json:"ValorTotalMercadorias"`
	ValorTotalFrete                     float64	`json:"ValorTotalFrete"`
	ValorTotalSeguro                    float64	`json:"ValorTotalSeguro"`
	ValorTotalDespAcessoriaTributada    float64	`json:"ValorTotalDespAcessoriaTributada"`
	ValorBaseCalculoIpi                 float64	`json:"ValorBaseCalculoIpi"`
	ValorTotalIpi                       float64	`json:"ValorTotalIpi"`
	ValorTotalDescontos                 float64	`json:"ValorTotalDescontos"`
	ValorTotalNota                      float64	`json:"ValorTotalNota"`
	ValotTotalDespAcessoriaNaoTributada float64	`json:"ValotTotalDespAcessoriaNaoTributada"`
	Filler                              string 	`json:"Filler"`
}
