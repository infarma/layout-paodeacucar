Layout de Pedido de Compra

    Registro Header:
    
        gerador_de_layouts pedidoDeCompra registroHeader CodRegistro:int64:0:2 NumeroPedido:int64:2:17 TipoPedido:string:17:20 DataEmissaoPedido:int64:20:28 DataEntregaInicial:int64:28:36 DataEntregaFinal:int64:36:44 IdentificacaoListaDePreco:string:44:58 CodEanComprador:string:58:71 CodEanFornecedor:string:71:84 CodEanLocalEntrega:string:84:97 CodEanLocalCobranca:string:97:110 TipoFrete:string:110:113 ValorTotalIpi:float64:113:126:2 CodFornecedor:int64:126:131 CodBarraCgcFornecedor:int64:131:135 TipoDocumento:string:135:138 DataEmissaoDaMensagem:int64:138:146 HoraEmissaoDaMensagem:int64:146:150 NomeDoFornecedor:string:150:165 Filler:string:165:350
        
    Registro Observação:
    
        gerador_de_layouts pedidoDeCompra registroObservacao CodRegistro:int64:0:2 NumeroPedido:int64:2:17 ObservacaoPedido:string:17:157 Filler:string:157:350
        
    Registro Condições de Pagamento
    
        gerador_de_layouts pedidoDeCompra registroCondicoesDePagamento CodRegistro:int64:0:2 NumeroPedido:int64:2:17 TipoPagamento:string:17:20 ReferênciaDePrazo:int64:20:21 DescricaoCondPagto:string:21:23 QtdeDias:int64:23:26 DataDeVencimento:int64:26:34 PorcentagemDescontoFinanceiro:float64:34:40:3 PorcentagemAPagarFatura:float64:40:45:3 Filler:string:45:350                
        
    Registro Detalhe        
    
        gerador_de_layouts pedidoDeCompra registroDetalhe CodRegistro:int64:0:2 NumeroDoPedido:int64:2:17 TipoCodProduto:string:17:20 CodEanProduto:int64:20:34 UnidadesNaEmbalagem:int64:34:40 DescricaoProduto:string:40:75 UnidadeMedida:string:75:78 QtdeSolicitada:float64:78:87:3 ValorBrutoUnitario:float64:87:104:6 PercentualBonificação:float64:104:110:4 ValorIpi:float64:110:123:2 ValorDespAcessoriasTributadas:float64:123:136:2 ValorDespAcessoriasNaoTributadas:float64:136:149:2 ValorIcmFonte:float64:149:162:2 ValorFrete:float64:162:175:2 CodigoReferencia:string:175:190 CodigoInternoPack:int64:190:196 ValorDescontoNegociado:float64:196:211:7 FormaPagamentoDesconto:int64:211:213 DescontoComercial:float64:213:219:4 ReservadoParaTextil0:int64:219:226 ReservadoParaTextil1:int64:226:237 ReservadoParaTextil2:int64:237:242 ReservadoParaTextil3:int64:242:262 ReservadoParaTextil4:int64:262:275 ReservadoParaTextil5:int64:275:285 ReservadoParaTextil6:int64:285:300 ValorBaseCalcRessarcSt:int64:300:315 ValorIcmsRessarcSt:int64:315:330 Filler:string:330:350
        
    Registro Grade
    
        gerador_de_layouts pedidoDeCompra registroGrade CodRegistro:int64:0:2 NumeroPedido:int64:2:17 CodEanProduto:int64:17:31 CodEanLoja:int64:31:44 QtdeUnSolicitada:int64:44:52 QtdePackSolicitada:int64:52:61 CodigoInternoPack:int64:61:68 Filler:string:68:350      
        
    Registro Componente
    
        gerador_de_layouts pedidoDeCompra registroComponente CodRegistro:int64:0:2 NumeroPedido:int64:2:17 CodEanProduto:int64:17:31 DescricaoCor:string:31:51 DescricaoTamanho:string:51:71 QtdeSolicitada:float64:71:80:3 CodigoInternoPack:int64:80:86 QtdePackSolicitada:float64:86:95:3 CodEanCorTamanho:int64:95:108 Filler:int64:108:350
        
    Registro Trailer
    
        gerador_de_layouts pedidoDeCompra registroTrailer CodRegistro:int64:0:2 NumeroPedido:int64:2:17 ValorTotalDespAcessoriasTributadas:float64:17:30:2 ValorTotalMercadorias:float64:30:47:6 ValorTotalBonificacao:float64:47:60:2 ValorTotalPedido:float64:60:77:6 ValorTotalDespAcessoriasNaoTributadas:float64:77:90:2 ValorTotalIcmFonte:float64:90:103:2 ValorTotalFrete:float64:103:116:2 Filler:string:116:350
        
Layout Resposta de Pedido de Compra:

    Registro Header:
        
        gerador_de_layouts respostaPedidoDeCompra registroHeader CodRegistro:int64:0:2 NumeroPedidoCbdSendoRespondido:int64:2:18 TipoAcaoEmRelacaoAoPedidoCbd:string:18:19 DataEnrtregaPedido:int64:19:27 CodEanCompradorCbd:string:27:40 CodEanLocalEntrega:string:40:53 Filler:string:53:66   
        
    Registro Detalhe:
    
        gerador_de_layouts respostaPedidoDeCompra registroDetalhe CodRegistro:int64:0:2 NumeroPedidoCbdSendoRespondido:int64:2:18 TipoCodProduto:string:18:21 CodEanProduto:int64:21:35 QtdeQueSeraAtendidaPeloFornecedor:int64:35:44 CodigoDoMotivoDoNaoAtendimento:string:44:49 Filler:string:49:81

    Registro Trailer:
    
        gerador_de_layouts respostaPedidoDeCompra registroTrailer CodRegistro:int64:0:2 Filler:string:2:81
        
Layout Nota Fiscal Invoic

    Registro Header 1:
    
        gerador_de_layouts notaFiscalInvoic registroHeader1 CodRegistro:int64:0:2 FuncaoMensagem:string:2:3 NumeroNotaFiscal:int64:3:12 CodSerieNota:int64:12:15 DataHoraEmissaoNota:int64:15:27 DataHoraSaidaNota:int64:27:39 DataHoraPrevisaoEntrega:int64:39:51 CodFiscalOperacao:int64:51:55 NumeroPedido1:int64:55:70 NumeroPedido2:int64:70:85 NumeroPedido3:int64:85:100 CodEanComprador:int64:100:113 CodEanEmissorFatura:int64:113:126 NumCgcEmissorFatura:int64:126:141 NumInscricaoEstadualEmissorFatura:string:141:161 CodUfInscrEstEmissorFatura:string:161:163 CodEanLocalEntrega:string:163:176 CodEanLocalCobranca:string:176:189 CodBanco:int64:189:193 CodAgenciaBancaria:int64:193:198 NumContaCorrente:int64:198:209 Filler:string:209:280  

    Registro Condicoes de Pagamento
    
        gerador_de_layouts notaFiscalInvoic registroCondicoesDePagamento CodRegistro:int64:0:2 NumeroNotaFiscal:int64:2:11 CodSerieNota:int64:11:14 TipoDePagamento:int64:14:17 ReferenciaDePrazo:int64:17:18 DescricaoCondPagto:string:18:20 QtdeDiasCondPagto:int64:20:23 DataDeVencimento:int64:23:31 PorcentagemDescontoFinanceiro:float64:31:37:3 PorcentagemAPagarFatura:float64:37:42:2 Filler:string:42:280
        
    Registro Header 3:
    
        gerador_de_layouts notaFiscalInvoic registroHeader3 CodRegistro:int64:0:2 NumeroNotaFiscal:int64:2:11 CodSerieNota:int64:11:14 TipoVeiculo:string:14:18 CodEanOuCgcTransportadora:int64:18:33 TipoIdentificacaoTransp:string:33:36 NomeTransportadora:string:36:61 IdentPlacaVeiculo:string:61:73 TipoFrete:string:73:76 ValorEncargosFinanceiros:float64:76:89:2 TaxaAliquotaIcmsFrete:float64:89:94:2 TaxaAliquotaIcmsSeguro:float64:94:99:2 Filler:string:99:280
        
    Registro Detalhe:
    
        gerador_de_layouts notaFiscalInvoic registroDetalhe CodRegistro:int64:0:2 NumeroNotaFiscal:int64:2:11 CodSerieNota:int64:11:14 CodEanProduto:int64:14:28 VolumeTotal:float64:28:38:3 QtdeFaturada:float64:38:48:3 UnidadeMedidaFaturada:int64:48:51 QtdeEntrega:float64:51:61:3 UnidadeMedidaEntregue:string:61:64 PesoTotalItemEntrega:float64:64:72:3 ValorBrutoTotal:float64:72:87:2 ValorLiquidoTotal:float64:87:102:2 ValorBrutoUnitario:float64:102:115:4 ValorLiquidoUnitario:int64:115:128:4 CodSituacaoTributaria:string:128:130 TaxaAliquotaIcms:float64:130:135:2 TaxaAliquotaIcsmSubstituicaoTributaria:float64:135:140:2 TaxaAliquotaIpi:float64:140:145:2 TaxaPercentualDescontoComercial:float64:145:150:2 ValorDescontoComercial:float64:150:163:2 PorcentagemReducaoBaseIcms:float64:163:168:2 ValorDeBaseDeCalculoDeIcms:float64:168:183:2 ValorDoIcmsCalculado:float64:183:198:2 ValorDeBaseDeCalculoDeIcmsSt:float64:198:213:2 ValorDoIcmsClaculadoSt:float64:213:228:2
        
    Registro Trailer:
    
        gerador_de_layouts notaFiscalInvoic registroTrailer CodRegistro:int64:0:2  NumeroNotaFiscal:int64:2:11 CodSerieNota:int64:11:14 NumTotalItensNota:int64:14:18 NumTotalEmbalagens:int64:18:22 QtdeTotalPallets:int64:22:37 TotalPesoBruto:int64:37:46 TotalPesoLiquido:float64:46:54:3 ValorBaseDeCalculoIcms:float64:54:70:2 ValorTotalIcms:float64:70:85:2 ValorBaseCalculoIcmsSubstituicao:float64:85:101:2 ValorTotalIcmsSubstituicao:float64:101:116:2 ValorTotalMercadorias:float64:116:131:2 ValorTotalFrete:float64:131:144:2 ValorTotalSeguro:float64:144:157:2 ValorTotalDespAcessoriaTributada:float64:157:170:2 ValorBaseCalculoIpi:float64:170:186:2 ValorTotalIpi:float64:186:201:2 ValorTotalDescontos:float64:201:216:2 ValorTotalNota:float64:216:231:2 ValotTotalDespAcessoriaNaoTributada:float64:231:244:2 Filler:string:244:280
        
          