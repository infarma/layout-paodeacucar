package respostaPedidoDeCompra

import (
	"fmt"
	"regexp"
	"strings"
	"time"
)

// Arquivo de Retorno de Pedido - Itens Atendidos
type ArquivoDeRespostaPedido struct {
	RegistroHeader  RegistroHeader    `json:"RegistroHeader"`
	RegistroDetalhe []RegistroDetalhe `json:"RegistroDetalhe"`
	RegistroTrailer RegistroTrailer   `json:"RegistroTrailer"`
}

// Registro Tipo 1 - Heander
func HeaderResposta(NumPedido int64, TipoAcao string, DataEntrega time.Time, CodEanComprador string, CodEanFornecedor string, CodEanLocal string) string {
	header := fmt.Sprint("01")
	header += fmt.Sprintf("%015d", NumPedido)
	header += fmt.Sprintf("%s", TipoAcao)
	str := DataEntrega.String()
	x := str[0:10]
	re := regexp.MustCompile(`:|-|\s`)
	data := re.ReplaceAllLiteralString(x, "")
	header += fmt.Sprintf("%08s", data)
	header += fmt.Sprintf("%013s", CodEanComprador)
	header += fmt.Sprintf("%013s", CodEanFornecedor)
	header += fmt.Sprintf("%013s", CodEanLocal)
	header += fmt.Sprintf("               ")

	return header
}

// Registro Tipo 2 - Detalhe
func DetalheResposta( NumeroPedido int64, TipoCod string, CodEanProd int64, Qtde float64, CodigoDoMotivo string) string {
	detalhe := fmt.Sprint("03")
	detalhe += fmt.Sprintf("%015d", NumeroPedido)
	detalhe += fmt.Sprintf("%03s", TipoCod)
	detalhe += fmt.Sprintf("%014d", CodEanProd)
	detalhe += fmt.Sprintf("%09s", strings.Replace(fmt.Sprintf("%.2f", Qtde), ".", "", 1))
	detalhe += fmt.Sprintf("%05s", CodigoDoMotivo)
	detalhe += fmt.Sprintf("                                ")
	return detalhe
}

// Registro Tipo 3 - Trailer
func TrailerResposta() string {
	Trailer := fmt.Sprint("09")
	Trailer += fmt.Sprintf("                                                                              ")
	return Trailer
}


