package respostaPedidoDeCompra

type RegistroTrailer struct {
	CodRegistro int64 	`json:"CodRegistro"`
	Filler      string	`json:"Filler"`
}
