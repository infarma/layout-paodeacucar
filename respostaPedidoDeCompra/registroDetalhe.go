package respostaPedidoDeCompra

type RegistroDetalhe struct {
	CodRegistro                       int64 	`json:"CodRegistro"`
	NumeroPedidoCbdSendoRespondido    int64 	`json:"NumeroPedidoCbdSendoRespondido"`
	TipoCodProduto                    string	`json:"TipoCodProduto"`
	CodEanProduto                     int64 	`json:"CodEanProduto"`
	QtdeQueSeraAtendidaPeloFornecedor float64 	`json:"QtdeQueSeraAtendidaPeloFornecedor"`
	CodigoDoMotivoDoNaoAtendimento    string	`json:"CodigoDoMotivoDoNaoAtendimento"`
	Filler                            string	`json:"Filler"`
}
