package respostaPedidoDeCompra

import "time"

type RegistroHeader struct {
	CodRegistro                    int64 	`json:"CodRegistro"`
	NumeroPedidoCbdSendoRespondido int64 	`json:"NumeroPedidoCbdSendoRespondido"`
	TipoAcaoEmRelacaoAoPedidoCbd   string	`json:"TipoAcaoEmRelacaoAoPedidoCbd"`
	DataEnrtregaPedido             time.Time 	`json:"DataEnrtregaPedido"`
	CodEanCompradorCbd             string	`json:"CodEanCompradorCbd"`
	CodEanFornecedor               string   `json:"CodEanFornecedor"`
	CodEanLocalEntrega             string	`json:"CodEanLocalEntrega"`
	Filler                         string	`json:"Filler"`
}
