package layout_paodeacucar

import (
	"bitbucket.org/infarma/layout-paodeacucar/pedidoDeCompra"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (pedidoDeCompra.ArquivoDePedido, error) {
	return pedidoDeCompra.GetStruct(fileHandle)
}
