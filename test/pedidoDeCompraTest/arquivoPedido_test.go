	package pedidoDeCompraTest

	import (
		"fmt"
		"layout-paodeacucar/pedidoDeCompra"
		"os"
		"strconv"
		"testing"
	)

	func TestGetStruct01(t *testing.T) {

		file, err := os.Open("../testFIle/ORDERS.SCP")
		if err != nil {
			t.Error("não abriu arquivo", err)
		}

		arquivo,err2 := pedidoDeCompra.GetStruct(file)
		//REGISTRO TIPO 1 ( HEADER)
		var header pedidoDeCompra.RegistroHeader
		header.CodRegistro = 1
		header.NumeroPedido  = 102007499760018
		header.TipoPedido = "870"
		header.DataEmissaoPedido = 13852001
		header.DataEntregaInicial= 14190420
		header.DataEntregaFinal= 17100914
		header.IdentificacaoListaDePreco= "53000000000000"
		header.CodEanComprador = "1000000000000"
		header.CodEanFornecedor = "1000000000000"
		header.CodEanLocalEntrega = "1000000000000"
		header.CodEanLocalCobranca = "1000000000000"
		header.TipoFrete = "111"
		header.ValorTotalIpi = 22222222222.11
		header.CodFornecedor = 55555
		header.CodBarraCgcFornecedor = 4444
		header.TipoDocumento = "333"
		header.DataEmissaoDaMensagem = 20171009
		header.HoraEmissaoDaMensagem = 2017
		header.NomeDoFornecedor = "100000000000001"
		header.Filler = "10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001"


		if header.CodRegistro != arquivo.RegistroHeader.CodRegistro {
			t.Error("CodRegistro não é compativel", err2)
		}
		if header.NumeroPedido != arquivo.RegistroHeader.NumeroPedido {
			t.Error("NumeroPedido não é compativel", err2)
		}
		if header.TipoPedido != arquivo.RegistroHeader.TipoPedido {
			t.Error("TipoPedido não é compativel", err2)
		}
		if header.DataEmissaoPedido != arquivo.RegistroHeader.DataEmissaoPedido {
			t.Error("DataEmissaoPedido não é compativel", err2)
		}
		if header.DataEntregaInicial != arquivo.RegistroHeader.DataEntregaInicial {
			t.Error("DataEntregaInicial não é compativel", err2)
		}
		if header.DataEntregaFinal != arquivo.RegistroHeader.DataEntregaFinal {
			t.Error("DataEntregaFinal não é compativel", err2)
		}
		if header.IdentificacaoListaDePreco != arquivo.RegistroHeader.IdentificacaoListaDePreco {
			t.Error("IdentificacaoListaDePreco não é compativel", err2)
		}
		if header.CodEanComprador != arquivo.RegistroHeader.CodEanComprador {
			t.Error("CodEanComprador não é compativel", err2)
		}
		if header.CodEanFornecedor != arquivo.RegistroHeader.CodEanFornecedor {
			t.Error("CodEanFornecedor não é compativel", err2)
		}
		if header.CodEanLocalEntrega != arquivo.RegistroHeader.CodEanLocalEntrega {
			t.Error("CodEanLocalEntrega não é compativel", err2)
		}
		if header.CodEanLocalCobranca != arquivo.RegistroHeader.CodEanLocalCobranca {
			t.Error("CodEanLocalCobranca não é compativel", err2)
		}
		if header.TipoFrete != arquivo.RegistroHeader.TipoFrete {
			t.Error("TipoFrete não é compativel", err2)
		}
		if header.ValorTotalIpi != arquivo.RegistroHeader.ValorTotalIpi {
			t.Error("ValorTotalIpi não é compativel", err2)
		}
		if header.CodFornecedor != arquivo.RegistroHeader.CodFornecedor {
			t.Error("CodFornecedor não é compativel", err2)
		}
		if header.CodBarraCgcFornecedor != arquivo.RegistroHeader.CodBarraCgcFornecedor {
			t.Error("CodBarraCgcFornecedor não é compativel", err2)
		}
		if header.TipoDocumento != arquivo.RegistroHeader.TipoDocumento {
			t.Error("TipoDocumento não é compativel", err2)
		}
		if header.DataEmissaoDaMensagem != arquivo.RegistroHeader.DataEmissaoDaMensagem {
			t.Error("DataEmissaoDaMensagem não é compativel", err2)
		}
		if header.HoraEmissaoDaMensagem != arquivo.RegistroHeader.HoraEmissaoDaMensagem {
			t.Error("HoraEmissaoDaMensagem não é compativel", err2)
		}
		if header.NomeDoFornecedor != arquivo.RegistroHeader.NomeDoFornecedor {
			t.Error("NomeDoFornecedor não é compativel", err2)
		}
		if header.Filler != arquivo.RegistroHeader.Filler {
			t.Error("Filler não é compativel", err2)
		}

	// case 2 : RegistroCondicoesDePagamento

		var rcp pedidoDeCompra.RegistroCondicoesDePagamento
		rcp.CodRegistro = 02
		rcp.NumeroPedido  = 102007499760018
		rcp.TipoPagamento = "870"
		rcp.ReferênciaDePrazo  = 1
		rcp.DescricaoCondPagto = "38"
		rcp.QtdeDias  = 520
		rcp.DataDeVencimento  = 11141904
		rcp.PorcentagemDescontoFinanceiro = 201.71
		rcp.PorcentagemAPagarFatura = 9.145
		rcp.Filler = "30000000000001000000000000100000000000010000000000001000000000000111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"

		if rcp.CodRegistro != arquivo.RegistroCondicoesDePagamento.CodRegistro {
			t.Error("CodRegistro não é compativel", err2)
		}
		if rcp.NumeroPedido != arquivo.RegistroCondicoesDePagamento.NumeroPedido {
			t.Error("NumeroPedido não é compativel", err2)
		}
		if rcp.TipoPagamento != arquivo.RegistroCondicoesDePagamento.TipoPagamento {
			t.Error("TipoPagamento não é compativel", err2)
		}
		if rcp.ReferênciaDePrazo != arquivo.RegistroCondicoesDePagamento.ReferênciaDePrazo {
			t.Error("ReferênciaDePrazo não é compativel", err2)
		}
		if rcp.DescricaoCondPagto != arquivo.RegistroCondicoesDePagamento.DescricaoCondPagto {
			t.Error("DescricaoCondPagto não é compativel", err2)
		}
		if rcp.QtdeDias != arquivo.RegistroCondicoesDePagamento.QtdeDias {
			t.Error("QtdeDias não é compativel", err2)
		}
		if rcp.DataDeVencimento != arquivo.RegistroCondicoesDePagamento.DataDeVencimento {
			t.Error("DataDeVencimento não é compativel")
		}
		if rcp.PorcentagemDescontoFinanceiro != arquivo.RegistroCondicoesDePagamento.PorcentagemDescontoFinanceiro {
			t.Error("PorcentagemDescontoFinanceiro não é compativel")
		}
		if rcp.PorcentagemAPagarFatura != arquivo.RegistroCondicoesDePagamento.PorcentagemAPagarFatura {
			t.Error("PorcentagemAPagarFatura não é compativel")
		}
		if rcp.Filler != arquivo.RegistroCondicoesDePagamento.Filler {
			t.Error("Filler não é compativel")
		}

		//case 3 : RegistroDetalhe

		reservadoParaTextil4, _ := strconv.ParseInt("2222222222222", 10, 64)

		var detalhe pedidoDeCompra.RegistroDetalhe
		detalhe.CodRegistro = 03
		detalhe.NumeroDoPedido = 102007499760018
		detalhe.TipoCodProduto  = "870"
		detalhe.CodEanProduto   = 13852001141904
		detalhe.UnidadesNaEmbalagem = 201710
		detalhe.DescricaoProduto  = "00000000010000000000001000000000000"
		detalhe.UnidadeMedida = "111"
		detalhe.QtdeSolicitada = 222222.222
		detalhe.ValorBrutoUnitario = 22115555544.443332
		detalhe.PercentualBonificação = 11.7111
		detalhe.ValorIpi = 92017100000.01
		detalhe.ValorDespAcessoriasTributadas   = 92017100000.01
		detalhe.ValorDespAcessoriasNaoTributadas = 92017100000.01
		detalhe.ValorIcmFonte = 92017100000.01
		detalhe.ValorFrete = 92017100000.01
		detalhe.CodigoReferencia = "100000000000000"
		detalhe.CodigoInternoPack   = 100000
		detalhe.ValorDescontoNegociado = 10000000.0000001
		detalhe.FormaPagamentoDesconto = 11
		detalhe.DescontoComercial = 11.1111
		detalhe.ReservadoParaTextil0 = 7777777
		detalhe.ReservadoParaTextil1 = 666666666.66
		detalhe.ReservadoParaTextil2 = 55555
		detalhe.ReservadoParaTextil3 = "10000000000000000001"
		detalhe.ReservadoParaTextil4 = int64(reservadoParaTextil4)
		detalhe.ReservadoParaTextil5 = 9999999999
		detalhe.ReservadoParaTextil6 = 999999999999999
		detalhe.ValorBaseCalcRessarcSt = 9999999999999.99
		detalhe.ValorIcmsRessarcSt = 9999999999999.99
		detalhe.CodigoDoProjeto = 1000000
		detalhe.DescontoDoProjeto = 2222
		detalhe.Filler = "000000001"

		if detalhe.CodRegistro != arquivo.RegistroDetalhe[0].CodRegistro {
			t.Error("CodRegistro não é compativel")
		}
		if detalhe.NumeroDoPedido != arquivo.RegistroDetalhe[0].NumeroDoPedido {
			t.Error("NumeroDoPedido não é compativel")
		}
		if detalhe.TipoCodProduto != arquivo.RegistroDetalhe[0].TipoCodProduto {
			t.Error("TipoCodProduto não é compativel")
		}
		if detalhe.CodEanProduto != arquivo.RegistroDetalhe[0].CodEanProduto {
			t.Error("CodEanProduto não é compativel")
		}
		if detalhe.UnidadesNaEmbalagem != arquivo.RegistroDetalhe[0].UnidadesNaEmbalagem {
			t.Error("UnidadesNaEmbalagem não é compativel")
		}
		if detalhe.DescricaoProduto != arquivo.RegistroDetalhe[0].DescricaoProduto {
			t.Error("DescricaoProduto não é compativel")
		}
		if detalhe.UnidadeMedida != arquivo.RegistroDetalhe[0].UnidadeMedida {
			t.Error("UnidadeMedida não é compativel")
		}
		if detalhe.QtdeSolicitada != arquivo.RegistroDetalhe[0].QtdeSolicitada {
			t.Error("QtdeSolicitada não é compativel")
		}
		if detalhe.ValorBrutoUnitario != arquivo.RegistroDetalhe[0].ValorBrutoUnitario {
			t.Error("ValorBrutoUnitario não é compativel")
		}
		if detalhe.PercentualBonificação != arquivo.RegistroDetalhe[0].PercentualBonificação {
			t.Error("PercentualBonificação não é compativel")
		}
		if detalhe.ValorIpi != arquivo.RegistroDetalhe[0].ValorIpi {
			t.Error("ValorIpi não é compativel")
		}
		if detalhe.ValorDespAcessoriasTributadas != arquivo.RegistroDetalhe[0].ValorDespAcessoriasTributadas {
			t.Error("ValorDespAcessoriasTributadas não é compativel")
		}
		if detalhe.ValorDespAcessoriasNaoTributadas != arquivo.RegistroDetalhe[0].ValorDespAcessoriasNaoTributadas {
			t.Error("ValorDespAcessoriasNaoTributadas não é compativel")
		}
		if detalhe.ValorIcmFonte != arquivo.RegistroDetalhe[0].ValorIcmFonte {
			t.Error("ValorIcmFonte não é compativel")
		}
		if detalhe.ValorFrete != arquivo.RegistroDetalhe[0].ValorFrete {
			t.Error("ValorFrete não é compativel")
		}
		if detalhe.CodigoReferencia != arquivo.RegistroDetalhe[0].CodigoReferencia {
			t.Error("CodigoReferencia não é compativel")
		}
		if detalhe.CodigoInternoPack != arquivo.RegistroDetalhe[0].CodigoInternoPack {
			t.Error("CodigoInternoPack não é compativel")
		}
		if detalhe.ValorDescontoNegociado != arquivo.RegistroDetalhe[0].ValorDescontoNegociado {
			t.Error("ValorDescontoNegociado não é compativel")
		}
		if detalhe.FormaPagamentoDesconto != arquivo.RegistroDetalhe[0].FormaPagamentoDesconto {
			t.Error("FormaPagamentoDesconto não é compativel")
		}
		if detalhe.DescontoComercial != arquivo.RegistroDetalhe[0].DescontoComercial {
			t.Error("DescontoComercial não é compativel")
		}
		if detalhe.ReservadoParaTextil0 != arquivo.RegistroDetalhe[0].ReservadoParaTextil0 {
			t.Error("ReservadoParaTextil0 não é compativel")
		}
		if detalhe.ReservadoParaTextil1 != arquivo.RegistroDetalhe[0].ReservadoParaTextil1 {
			t.Error("ReservadoParaTextil1 não é compativel")
		}
		if detalhe.ReservadoParaTextil2 != arquivo.RegistroDetalhe[0].ReservadoParaTextil2 {
			t.Error("ReservadoParaTextil2 não é compativel")
		}
		if detalhe.ReservadoParaTextil3 != arquivo.RegistroDetalhe[0].ReservadoParaTextil3 {
			t.Error("ReservadoParaTextil3 não é compativel")
		}
		if detalhe.ReservadoParaTextil4 != arquivo.RegistroDetalhe[0].ReservadoParaTextil4 {
			t.Error("ReservadoParaTextil4 não é compativel")
		}
		if detalhe.ReservadoParaTextil5 != arquivo.RegistroDetalhe[0].ReservadoParaTextil5 {
			t.Error("ReservadoParaTextil5 não é compativel")
		}
		if detalhe.ReservadoParaTextil6 != arquivo.RegistroDetalhe[0].ReservadoParaTextil6 {
			t.Error("ReservadoParaTextil6 não é compativel")
		}
		if detalhe.ValorBaseCalcRessarcSt != arquivo.RegistroDetalhe[0].ValorBaseCalcRessarcSt {
			t.Error("ValorBaseCalcRessarcSt não é compativel")
		}
		if detalhe.ValorIcmsRessarcSt != arquivo.RegistroDetalhe[0].ValorIcmsRessarcSt {
			t.Error("ValorIcmsRessarcSt não é compativel")
		}
		if detalhe.CodigoDoProjeto != arquivo.RegistroDetalhe[0].CodigoDoProjeto {
			t.Error("CodigoDoProjeto não é compativel")
		}
		if detalhe.DescontoDoProjeto != arquivo.RegistroDetalhe[0].DescontoDoProjeto {
			t.Error("DescontoDoProjeto não é compativel")
		}
		if detalhe.Filler != arquivo.RegistroDetalhe[0].Filler {
			t.Error("Filler não é compativel")
		}


		// //case 4 : RegistroGrade QtdeUnSolicitada e  CodigoInternoPack foram alterados de int para float, pois o layout exigia casas decimáis

		var grade pedidoDeCompra.RegistroGrade

		grade.CodRegistro = 04
		grade.NumeroPedido  = 102007499760018
		grade.CodEanProduto = 87013852001141
		grade.CodEanLoja  = 9042017100914
		grade.QtdeUnSolicitada = 53000.000
		grade.QtdePackSolicitada = 153000.000
		grade.CodigoInternoPack  = 1666666
		grade.Filler = "000100000000000010000000000001000000000000111222222222221155555444433320171009201710000000000000110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001"

		if grade.CodRegistro != arquivo.RegistroGrade[0].CodRegistro {
			t.Error("CodRegistro não é compativel", err2)
		}
		if grade.NumeroPedido != arquivo.RegistroGrade[0].NumeroPedido {
			t.Error("NumeroPedido não é compativel", err2)
		}
		if grade.CodEanProduto != arquivo.RegistroGrade[0].CodEanProduto {
			t.Error("CodEanProduto não é compativel", err2)
		}
		if grade.CodEanLoja != arquivo.RegistroGrade[0].CodEanLoja {
			t.Error("CodEanLoja não é compativel", err2)
		}
		if grade.QtdeUnSolicitada != arquivo.RegistroGrade[0].QtdeUnSolicitada {
			fmt.Println("teste QtdeUnSolicitada", grade.QtdeUnSolicitada )
			fmt.Println("arquivo QtdeUnSolicitada", arquivo.RegistroGrade[0].QtdeUnSolicitada )
			t.Error("QtdeUnSolicitada não é compativel", err2)
		}
		if grade.QtdePackSolicitada != arquivo.RegistroGrade[0].QtdePackSolicitada {
			fmt.Println("teste QtdePackSolicitada", grade.QtdePackSolicitada )
			fmt.Println("arquivo QtdePackSolicitada", arquivo.RegistroGrade[0].QtdePackSolicitada )
			t.Error("QtdePackSolicitada não é compativel", err2)
		}
		if grade.CodigoInternoPack != arquivo.RegistroGrade[0].CodigoInternoPack {
			fmt.Println("teste CodigoInternoPack", grade.CodigoInternoPack )
			fmt.Println("arquivo CodigoInternoPack", arquivo.RegistroGrade[0].CodigoInternoPack )
			t.Error("CodigoInternoPack não é compativel", err2)
		}
		if grade.Filler != arquivo.RegistroGrade[0].Filler {
			t.Error("Filler não é compativel", err2)
		}

		//case 5 : RegistroComponente

		var componente pedidoDeCompra.RegistroComponente

		componente.CodRegistro = 05
		componente.NumeroPedido  = 102007499760018
		componente.CodEanProduto = 87013852001141
		componente.DescricaoCor = "90420171009145300000"
		componente.DescricaoTamanho = "01530000001666666000"
		componente.QtdeSolicitada  = 100000.000
		componente.CodigoInternoPack  = 100010
		componente.QtdePackSolicitada = 100000.000
		componente.CodEanCorTamanho  = 1010000000000
		componente.Filler  = "00111222222222221155555444433320171009201710000000000000110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001"

		if componente.CodRegistro != arquivo.RegistroComponente[0].CodRegistro {
			t.Error("CodRegistro não é compativel", err2)
		}
		if componente.NumeroPedido != arquivo.RegistroComponente[0].NumeroPedido {
			t.Error("NumeroPedido não é compativel", err2)
		}
		if componente.CodEanProduto != arquivo.RegistroComponente[0].CodEanProduto {
			t.Error("CodEanProduto não é compativel", err2)
		}
		if componente.DescricaoCor != arquivo.RegistroComponente[0].DescricaoCor {
			t.Error("DescricaoCor não é compativel", err2)
		}
		if componente.QtdeSolicitada != arquivo.RegistroComponente[0].QtdeSolicitada {
			t.Error("QtdeSolicitada não é compativel", err2)
		}
		if componente.CodigoInternoPack != arquivo.RegistroComponente[0].CodigoInternoPack {
			t.Error("CodigoInternoPack não é compativel", err2)
		}
		if componente.QtdePackSolicitada != arquivo.RegistroComponente[0].QtdePackSolicitada {
			t.Error("QtdePackSolicitada não é compativel", err2)
		}
		if componente.CodEanCorTamanho != arquivo.RegistroComponente[0].CodEanCorTamanho {
			t.Error("CodEanCorTamanho não é compativel", err2)
		}
		if componente.Filler != arquivo.RegistroComponente[0].Filler {
			t.Error("Filler não é compativel", err2)
		}

		//case 9 : RegistroTrailer

		var trailer pedidoDeCompra.RegistroTrailer

		trailer.CodRegistro = 9
		trailer.NumeroPedido  = 102007499760018
		trailer.ValorTotalDespAcessoriasTributadas = 87013852001.14
		trailer.ValorTotalMercadorias  = 19042017100.914530
		trailer.ValorTotalBonificacao = 10000153000.00
		trailer.ValorTotalPedido  = 11666666000.100000
		trailer.ValorTotalDespAcessoriasNaoTributadas = 10010001010.00
		trailer.ValorTotalIcmFonte  = 10000101000.00
		trailer.ValorTotalFrete  = 10000001112.22
		trailer.Filler = "222222221155555444433320171009201710000000000000110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001"

		if trailer.CodRegistro != arquivo.RegistroTrailer.CodRegistro {
			t.Error("CodRegistro não é compativel", err2)
		}
		if trailer.NumeroPedido != arquivo.RegistroTrailer.NumeroPedido {
			t.Error("NumeroPedido não é compativel", err2)
		}
		if trailer.ValorTotalDespAcessoriasTributadas != arquivo.RegistroTrailer.ValorTotalDespAcessoriasTributadas {
			t.Error("ValorTotalDespAcessoriasTributadas não é compativel", err2)
		}
		if trailer.ValorTotalMercadorias != arquivo.RegistroTrailer.ValorTotalMercadorias {
			t.Error("ValorTotalMercadorias não é compativel", err2)
		}
		if trailer.ValorTotalBonificacao != arquivo.RegistroTrailer.ValorTotalBonificacao {
			t.Error("ValorTotalBonificacao não é compativel", err2)
		}
		if trailer.ValorTotalPedido != arquivo.RegistroTrailer.ValorTotalPedido {
			t.Error("ValorTotalPedido não é compativel", err2)
		}
		if trailer.ValorTotalDespAcessoriasNaoTributadas != arquivo.RegistroTrailer.ValorTotalDespAcessoriasNaoTributadas {
			t.Error("ValorTotalDespAcessoriasNaoTributadas não é compativel", err2)
		}
		if trailer.ValorTotalIcmFonte != arquivo.RegistroTrailer.ValorTotalIcmFonte {
			t.Error("ValorTotalIcmFonte não é compativel", err2)
		}
		if trailer.ValorTotalFrete != arquivo.RegistroTrailer.ValorTotalFrete {
			t.Error("ValorTotalFrete não é compativel", err2)
		}
		if trailer.Filler != arquivo.RegistroTrailer.Filler {
			t.Error("Filler não é compativel", err2)
		}


	}