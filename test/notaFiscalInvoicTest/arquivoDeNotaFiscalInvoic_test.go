package notaFiscalInvoicTest

import (
	"fmt"
	"layout-paodeacucar/notaFiscalInvoic"
	"testing"
	"time"
)

func TestHeader1Nota(t *testing.T){

	 FuncaoMsg := ""
	 NumeroNf := int64(0)
	 CodSN := int64(0)
	 DataHoraEN := time.Date(2019, 10, 15, 12, 30, 0, 0, time.UTC)
	 DataHoraSN := time.Date(2019, 10, 15, 12, 30, 0, 0, time.UTC)
	 DataHoraPE := time.Date(2019, 10, 15, 12, 30, 0, 0, time.UTC)
	 CodFiscalOp := int64(0)
	 NumeroPed1 := int64(0)
	 NumeroPed2 := int64(0)
	 NumeroPed3 := int64(0)
	 CodEanComp := int64(0)
	 CodEanEm := int64(0)
	 NumCgcEm := int64(0)
	 NumInsEst := ""
	 CodUf := ""
	 CodEanLE := ""
	 CodEanLC := ""
	 CodBan := int64(0)
	 CodAgen := int64(0)
	 NumContaCorr := int64(0)

	header1 := notaFiscalInvoic.Header1Nota(FuncaoMsg, NumeroNf, CodSN, DataHoraEN, DataHoraSN, DataHoraPE, CodFiscalOp, NumeroPed1, NumeroPed2, NumeroPed3, CodEanComp, CodEanEm, NumCgcEm, NumInsEst, CodUf, CodEanLE, CodEanLC, CodBan, CodAgen, NumContaCorr) //entrar com os dados do banco

	if len(header1) != 280 {
		fmt.Println("header1", header1)
		t.Error("header1 não tem o tamanho adequado")
	}
	if header1 != "01000000000000020191015123020191015123020191015123000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                                                                       " {
		fmt.Println("header1 = ", header1)
		t.Error("header1 não é compativel")
	}
}

func TestCondicoesDePagamentoNota(t *testing.T){

	    NumeroNF := int64(4)
		CodSN := int64(22)
		TipoDePag := ""
		ReferenciaDePr := int64(0)
		DescricaoCP := ""
		QtdeDiasCP := int64(0)
		DataDeVenc := time.Date(2019, 10, 15, 12, 30, 0, 0, time.UTC)
		PorcentagemDF := float64(111.22)
		PorcentagemAPF := float64(333.333)

		Condicoes := notaFiscalInvoic.CondicoesDePagamentoNota(NumeroNF, CodSN, TipoDePag, ReferenciaDePr, DescricaoCP, QtdeDiasCP, DataDeVenc, PorcentagemDF, PorcentagemAPF) //entrar com os dados do banco

		if Condicoes != "02000000004022   0000002019101511122033333                                                                                                                                                                                                                                              " {
			fmt.Println("condicoes", Condicoes)
			t.Error("Condicoes não é compativel")
		}

}

func TestHeader3Nota(t *testing.T){

	NumeroNf := int64(1)
	CodSN := int64(1)
	TipoVeic := ""
	CodEan := int64(1)
	TipoIden := ""
	NomeTr := ""
	IdentPV := ""
	TipoF := ""
	ValorEF := float64(1.1)
	TaxaAlFrete := float64(111.11)
	TaxaAlSeguro := float64(111.11)


	header3 := notaFiscalInvoic.Header3Nota( NumeroNf, CodSN, TipoVeic, CodEan, TipoIden, NomeTr, IdentPV , TipoF, ValorEF, TaxaAlFrete, TaxaAlSeguro )

	if len(header3) != 280 {
		fmt.Println("header3", header3)
		t.Error("header3 não tem o tamanho adequado")
	}
	if header3 != "030000000010010000000000000000001000000000000000000000000000000000000000000000000000001101111111111                                                                                                                                                                                     " {
		fmt.Println("header3 = ", header3)
		t.Error("header3 não é compativel")
	}
}

func TestDetalheNota(t *testing.T){

	NumeroNotaFiscal := int64(1)
	CodSerieNota := int64(1)
	CodEanProduto := int64(1)
	VolumeTotal := float64(2222222.222)
	QtdeFaturada := float64(2222222.222)
	UnidadeMedidaFaturada := int64(1)
	QtdeEntrega := float64(2222222.222)
	UnidadeMedidaEntregue := ""
	PesoTotalItemEntrega := float64(22222.222)
	ValorBrutoTotal := float64(2222222222222.22)
	ValorLiquidoTotal := float64(2222222222222.22)
	ValorBrutoUnitario := float64(222222222.2222)
	ValorLiquidoUnitario := float64(222222222.2222)
	CodSituacaoTributaria := "77"
	TaxaAliquotaIcms := float64(333.22)
	TaxaAliquotaIcsmSubstituicaoTributaria := float64(333.22)
	TaxaAliquotaIpi := float64(333.22)
	TaxaPercentualDescontoComercial := float64(333.22)
	ValorDescontoComercial := float64(99999999999.22)
	PorcentagemReducaoBaseIcms := float64(333.22)
	ValorDeBaseDeCalculoDeIcms := float64(2222222222222.22)
	ValorDoIcmsCalculado := float64(2222222222222.22)
	ValorDeBaseDeCalculoDeIcmsSt := float64(2222222222222.22)
	ValorDoIcmsClaculadoSt := float64(2222222222222.22)
	CodigoNCM := int64(1)
	CodigoNCMEX := int64(1)
	CodigoCSTPIS := int64(1)
	CodigoCSTCOFINS := int64(1)
	CodigoCSTIPI := int64(1)
	CodigoCSTICMS := int64(1)
	TaxaAliquotaPIS := float64(444.21)
	TaxaAliquotaCOFINS := float64(444.02)
	TaxaMvaIcmsST := float64(444.20)



	detalhe := notaFiscalInvoic.DetalheNota(NumeroNotaFiscal, CodSerieNota, CodEanProduto, VolumeTotal, QtdeFaturada, UnidadeMedidaFaturada, QtdeEntrega, UnidadeMedidaEntregue, PesoTotalItemEntrega, ValorBrutoTotal, ValorLiquidoTotal, ValorBrutoUnitario, ValorLiquidoUnitario, CodSituacaoTributaria, TaxaAliquotaIcms, TaxaAliquotaIcsmSubstituicaoTributaria, TaxaAliquotaIpi, TaxaPercentualDescontoComercial, ValorDescontoComercial, PorcentagemReducaoBaseIcms, ValorDeBaseDeCalculoDeIcms, ValorDoIcmsCalculado , ValorDeBaseDeCalculoDeIcmsSt, ValorDoIcmsClaculadoSt, CodigoNCM, CodigoNCMEX, CodigoCSTPIS, CodigoCSTCOFINS, CodigoCSTIPI, CodigoCSTICMS, TaxaAliquotaPIS, TaxaAliquotaCOFINS, TaxaMvaIcmsST)

	if len(detalhe) != 280 {
		fmt.Println("detalhe", detalhe)
		t.Error("detalhe não tem o tamanho adequado")
	}
	if detalhe != "040000000010010000000000000122222222222222222222001222222222200022222222222222222222222222222222222222222222222222222222222222227733322333223332233322999999999992233322222222222222222222222222222222222222222222222222222222222222000000010101010101444214440244420                   " {
		fmt.Println("detalhe = ", detalhe)
		t.Error("detalhe não é compativel")
	}
}

func TestTrailerNota(t *testing.T){

	NumeroNotaFiscal := int64(1)
	CodSerieNota := int64(2)
	NumTotalItensNota := int64(3)
	NumTotalEmbalagens := int64(4)
	QtdeTotalPallets := float64(1111111111111.22)
	TotalPesoBruto := float64(666666.333)
	TotalPesoLiquido := float64(55555.333)
	ValorBaseDeCalculoIcms := float64( 11111111111111.11)
	ValorTotalIcms := float64( 1111111111111.11)
	ValorBaseCalculoIcmsSubstituicao := float64(11111111111111.11)
	ValorTotalIcmsSubstituicao := float64( 1111111111111.11)
	ValorTotalMercadorias := float64( 1111111111111.11)
	ValorTotalFrete := float64( 11111111111.11)
	ValorTotalSeguro := float64( 11111111111.11)
	ValorTotalDespAcessoriaTributada := float64( 11111111111.11)
	ValorBaseCalculoIpi := float64( 11111111111111.11)
	ValorTotalIpi := float64( 1111111111111.11)
	ValorTotalDescontos := float64( 1111111111111.11)
	ValorTotalNota := float64( 1111111111111.11)
	ValotTotalDespAcessoriaNaoTributada := float64( 11111111111.11)


	Trailer := notaFiscalInvoic.TrailerNota( NumeroNotaFiscal, CodSerieNota, NumTotalItensNota, NumTotalEmbalagens, QtdeTotalPallets, TotalPesoBruto, TotalPesoLiquido, ValorBaseDeCalculoIcms, ValorTotalIcms, ValorBaseCalculoIcmsSubstituicao, ValorTotalIcmsSubstituicao, ValorTotalMercadorias, ValorTotalFrete, ValorTotalSeguro, ValorTotalDespAcessoriaTributada, ValorBaseCalculoIpi, ValorTotalIpi, ValorTotalDescontos, ValorTotalNota, ValotTotalDespAcessoriaNaoTributada)


	if len(Trailer) != 280 {
		fmt.Println("Trailer", Trailer)
		t.Error("Trailer não tem o tamanho adequado")
	}
	if Trailer != "0900000000100200030004111111111111122666666333555553331111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111                                    " {
		fmt.Println("Trailer = ", Trailer)
		t.Error("Trailer não é compativel")
	}
}
