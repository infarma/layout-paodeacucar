package respostaPedidoDeCompraTest

import (
	"layout-paodeacucar/respostaPedidoDeCompra"
	"strconv"
	"testing"
	"time"
)

func TestHeaderResposta(t *testing.T){

	num, _ := strconv.ParseInt("011484470162750", 10, 64)
	NumPedido := int64(num)
	TipoAcao := "P"
	DataEntrega := time.Date(2019, 10, 15, 12, 30, 0, 0, time.UTC)
	CodEanComprador := "7895000000001"
	CodEanFornecedor :="7892445567711"
	CodEanLocal := "7895000027507"

	num2, _ := strconv.ParseInt("", 10, 64)
	NumPedido2 := int64(num2)
	TipoAcao2 := "0"
	DataEntrega2 := time.Date(2019, 10, 15, 12, 30, 0, 0, time.UTC)
	CodEanComprador2 := ""
	CodEanFornecedor2 :=""
	CodEanLocal2 := ""

	header := respostaPedidoDeCompra.HeaderResposta(NumPedido, TipoAcao, DataEntrega, CodEanComprador, CodEanFornecedor, CodEanLocal)//entrar com os dados do banco

	header2 := respostaPedidoDeCompra.HeaderResposta(NumPedido2, TipoAcao2, DataEntrega2, CodEanComprador2, CodEanFornecedor2, CodEanLocal2)//entrar com os dados do banco

	if len(header) != 80 {
		t.Error("Header não tem o tamanho adequado")
	}
	if header != "01011484470162750P20191015789500000000178924455677117895000027507               " {
		t.Error("Header não é compativel")
	}

	if len(header2) != 80 {
		t.Error("Header vazia não tem o tamanho adequado")
	}
	if header2 != "01000000000000000020191015000000000000000000000000000000000000000               " {
		t.Error("Header vazia não é compativel")
	}

}

func TestDetalheResposta(t *testing.T){

	num, _ := strconv.ParseInt("011484470162750", 10, 64)
	NumeroPedidoCbdSendoRespondido := int64(num)
	TipoCodProduto := "EAN"
	cod, _ := strconv.ParseInt("07896714218267", 10, 64)
	CodEanProduto :=  int64(cod)
	QtdeQueSeraAtendidaPeloFornecedor := 000000.000
	CodigoDoMotivoDoNaoAtendimento := "OW555"

	num2, _ := strconv.ParseInt("", 10, 64)
	NumeroPedidoCbdSendoRespondido2 := int64(num2)
	TipoCodProduto2 := ""
	cod2, _ := strconv.ParseInt("", 10, 64)
	CodEanProduto2 :=  int64(cod2)
	QtdeQueSeraAtendidaPeloFornecedor2 := 0.0
	CodigoDoMotivoDoNaoAtendimento2 := ""

	detalhe := respostaPedidoDeCompra.DetalheResposta(NumeroPedidoCbdSendoRespondido, TipoCodProduto, CodEanProduto, QtdeQueSeraAtendidaPeloFornecedor, CodigoDoMotivoDoNaoAtendimento )//entrar com os dados do banco

	detalhe2 := respostaPedidoDeCompra.DetalheResposta(NumeroPedidoCbdSendoRespondido2, TipoCodProduto2, CodEanProduto2, QtdeQueSeraAtendidaPeloFornecedor2, CodigoDoMotivoDoNaoAtendimento2 )//entrar com os dados do banco

	if detalhe != "03011484470162750EAN07896714218267000000000OW555                                " {
		t.Error("Detalhe não é compativel")
	}

	if detalhe2 != "030000000000000000000000000000000000000000000000                                " {
		t.Error("Detalhe vazio não é compativel")
	}

}

func TestTrailerResposta(t *testing.T){

	trailer := respostaPedidoDeCompra.TrailerResposta()//entrar com os dados do banco

	if trailer != "09                                                                              " {
		t.Error("Trailer não é compativel")
	}

}


