package pedidoDeCompra

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroGrade struct {
	CodRegistro        int64 	`json:"CodRegistro"`
	NumeroPedido       int64 	`json:"NumeroPedido"`
	CodEanProduto      int64 	`json:"CodEanProduto"`
	CodEanLoja         int64 	`json:"CodEanLoja"`
	QtdeUnSolicitada   float64 	`json:"QtdeUnSolicitada"`
	QtdePackSolicitada float64 	`json:"QtdePackSolicitada"`
	CodigoInternoPack  int64 	`json:"CodigoInternoPack"`
	Filler             string	`json:"Filler"`
}

func (r *RegistroGrade) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroGrade

	err = posicaoParaValor.ReturnByType(&r.CodRegistro, "CodRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodEanProduto, "CodEanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodEanLoja, "CodEanLoja")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QtdeUnSolicitada, "QtdeUnSolicitada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QtdePackSolicitada, "QtdePackSolicitada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoInternoPack, "CodigoInternoPack")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroGrade = map[string]gerador_layouts_posicoes.Posicao{
	"CodRegistro":                      {0, 2, 0},
	"NumeroPedido":                      {2, 17, 0},
	"CodEanProduto":                      {17, 31, 0},
	"CodEanLoja":                      {31, 44, 0},
	"QtdeUnSolicitada":                      {44, 52, 3},
	"QtdePackSolicitada":                      {52, 61, 3},
	"CodigoInternoPack":                      {61, 68, 0},
	"Filler":                      {68, 350, 0},
}