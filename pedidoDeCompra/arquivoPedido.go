package pedidoDeCompra

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	RegistroHeader               RegistroHeader               `json:"RegistroHeader"`
	RegistroObservacao           RegistroObservacao           `json:"RegistroObservacao"`
	RegistroCondicoesDePagamento RegistroCondicoesDePagamento `json:"RegistroCondicoesDePagamento"`
	RegistroDetalhe              []RegistroDetalhe            `json:"RegistroDetalhe"`
	RegistroGrade                []RegistroGrade              `json:"RegistroGrade"`
	RegistroComponente           []RegistroComponente         `json:"RegistroComponente"`
	RegistroTrailer              RegistroTrailer              `json:"RegistroTrailer"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		switch identificador {
		case "01":
			err = arquivo.RegistroHeader.ComposeStruct(string(runes))
		case "02":
			err = arquivo.RegistroCondicoesDePagamento.ComposeStruct(string(runes))
		case "03":
			var registroTemp RegistroDetalhe
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.RegistroDetalhe = append(arquivo.RegistroDetalhe, registroTemp)
		case "04":
			var registroTemp RegistroGrade
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.RegistroGrade = append(arquivo.RegistroGrade, registroTemp)
		case "05":
			var registroTemp RegistroComponente
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.RegistroComponente = append(arquivo.RegistroComponente, registroTemp)
		case "09":
			err = arquivo.RegistroTrailer.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
