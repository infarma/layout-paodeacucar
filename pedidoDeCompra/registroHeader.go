package pedidoDeCompra

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroHeader struct {
	CodRegistro               int64  	`json:"CodRegistro"`
	NumeroPedido              int64  	`json:"NumeroPedido"`
	TipoPedido                string 	`json:"TipoPedido"`
	DataEmissaoPedido         int64  	`json:"DataEmissaoPedido"`
	DataEntregaInicial        int64  	`json:"DataEntregaInicial"`
	DataEntregaFinal          int64  	`json:"DataEntregaFinal"`
	IdentificacaoListaDePreco string 	`json:"IdentificacaoListaDePreco"`
	CodEanComprador           string 	`json:"CodEanComprador"`
	CodEanFornecedor          string 	`json:"CodEanFornecedor"`
	CodEanLocalEntrega        string 	`json:"CodEanLocalEntrega"`
	CodEanLocalCobranca       string 	`json:"CodEanLocalCobranca"`
	TipoFrete                 string 	`json:"TipoFrete"`
	ValorTotalIpi             float64	`json:"ValorTotalIpi"`
	CodFornecedor             int64  	`json:"CodFornecedor"`
	CodBarraCgcFornecedor     int64  	`json:"CodBarraCgcFornecedor"`
	TipoDocumento             string 	`json:"TipoDocumento"`
	DataEmissaoDaMensagem     int64  	`json:"DataEmissaoDaMensagem"`
	HoraEmissaoDaMensagem     int64  	`json:"HoraEmissaoDaMensagem"`
	NomeDoFornecedor          string 	`json:"NomeDoFornecedor"`
	Filler                    string 	`json:"Filler"`
}

func (r *RegistroHeader) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroHeader

	err = posicaoParaValor.ReturnByType(&r.CodRegistro, "CodRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TipoPedido, "TipoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataEmissaoPedido, "DataEmissaoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataEntregaInicial, "DataEntregaInicial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataEntregaFinal, "DataEntregaFinal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.IdentificacaoListaDePreco, "IdentificacaoListaDePreco")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodEanComprador, "CodEanComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodEanFornecedor, "CodEanFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodEanLocalEntrega, "CodEanLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodEanLocalCobranca, "CodEanLocalCobranca")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TipoFrete, "TipoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalIpi, "ValorTotalIpi")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodFornecedor, "CodFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodBarraCgcFornecedor, "CodBarraCgcFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TipoDocumento, "TipoDocumento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataEmissaoDaMensagem, "DataEmissaoDaMensagem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.HoraEmissaoDaMensagem, "HoraEmissaoDaMensagem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NomeDoFornecedor, "NomeDoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroHeader = map[string]gerador_layouts_posicoes.Posicao{
	"CodRegistro":                      {0, 2, 0},
	"NumeroPedido":                      {2, 17, 0},
	"TipoPedido":                      {17, 20, 0},
	"DataEmissaoPedido":                      {20, 28, 0},
	"DataEntregaInicial":                      {28, 36, 0},
	"DataEntregaFinal":                      {36, 44, 0},
	"IdentificacaoListaDePreco":                      {44, 58, 0},
	"CodEanComprador":                      {58, 71, 0},
	"CodEanFornecedor":                      {71, 84, 0},
	"CodEanLocalEntrega":                      {84, 97, 0},
	"CodEanLocalCobranca":                      {97, 110, 0},
	"TipoFrete":                      {110, 113, 0},
	"ValorTotalIpi":                      {113, 126, 2},
	"CodFornecedor":                      {126, 131, 0},
	"CodBarraCgcFornecedor":                      {131, 135, 0},
	"TipoDocumento":                      {135, 138, 0},
	"DataEmissaoDaMensagem":                      {138, 146, 0},
	"HoraEmissaoDaMensagem":                      {146, 150, 0},
	"NomeDoFornecedor":                      {150, 165, 0},
	"Filler":                      {165, 350, 0},
}