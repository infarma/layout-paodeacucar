package pedidoDeCompra

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroDetalhe struct {
	CodRegistro                      int64  	`json:"CodRegistro"`
	NumeroDoPedido                   int64  	`json:"NumeroDoPedido"`
	TipoCodProduto                   string 	`json:"TipoCodProduto"`
	CodEanProduto                    int64  	`json:"CodEanProduto"`
	UnidadesNaEmbalagem              int64  	`json:"UnidadesNaEmbalagem"`
	DescricaoProduto                 string 	`json:"DescricaoProduto"`
	UnidadeMedida                    string 	`json:"UnidadeMedida"`
	QtdeSolicitada                   float64	`json:"QtdeSolicitada"`
	ValorBrutoUnitario               float64	`json:"ValorBrutoUnitario"`
	PercentualBonificação            float64	`json:"PercentualBonificação"`
	ValorIpi                         float64	`json:"ValorIpi"`
	ValorDespAcessoriasTributadas    float64	`json:"ValorDespAcessoriasTributadas"`
	ValorDespAcessoriasNaoTributadas float64	`json:"ValorDespAcessoriasNaoTributadas"`
	ValorIcmFonte                    float64	`json:"ValorIcmFonte"`
	ValorFrete                       float64	`json:"ValorFrete"`
	CodigoReferencia                 string 	`json:"CodigoReferencia"`
	CodigoInternoPack                int64  	`json:"CodigoInternoPack"`
	ValorDescontoNegociado           float64	`json:"ValorDescontoNegociado"`
	FormaPagamentoDesconto           int64  	`json:"FormaPagamentoDesconto"`
	DescontoComercial                float64	`json:"DescontoComercial"`
	ReservadoParaTextil0             int64  	`json:"ReservadoParaTextil0"`
	ReservadoParaTextil1             float64  	`json:"ReservadoParaTextil1"`
	ReservadoParaTextil2             int64  	`json:"ReservadoParaTextil2"`
	ReservadoParaTextil3             string  	`json:"ReservadoParaTextil3"`
	ReservadoParaTextil4             int64  	`json:"ReservadoParaTextil4"`
	ReservadoParaTextil5             int64  	`json:"ReservadoParaTextil5"`
	ReservadoParaTextil6             int64  	`json:"ReservadoParaTextil6"`
	ValorBaseCalcRessarcSt           float64  	`json:"ValorBaseCalcRessarcSt"`
	ValorIcmsRessarcSt               float64  	`json:"ValorIcmsRessarcSt"`
	CodigoDoProjeto                	 int64  	`json:"CodigoDoProjeto"`
	DescontoDoProjeto                int64  	`json:"DescontoDoProjeto"`
	Filler                           string 	`json:"Filler"`
}

func (r *RegistroDetalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroDetalhe

	err = posicaoParaValor.ReturnByType(&r.CodRegistro, "CodRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TipoCodProduto, "TipoCodProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodEanProduto, "CodEanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.UnidadesNaEmbalagem, "UnidadesNaEmbalagem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescricaoProduto, "DescricaoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.UnidadeMedida, "UnidadeMedida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QtdeSolicitada, "QtdeSolicitada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorBrutoUnitario, "ValorBrutoUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PercentualBonificação, "PercentualBonificação")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorIpi, "ValorIpi")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorDespAcessoriasTributadas, "ValorDespAcessoriasTributadas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorDespAcessoriasNaoTributadas, "ValorDespAcessoriasNaoTributadas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorIcmFonte, "ValorIcmFonte")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorFrete, "ValorFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoReferencia, "CodigoReferencia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoInternoPack, "CodigoInternoPack")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorDescontoNegociado, "ValorDescontoNegociado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.FormaPagamentoDesconto, "FormaPagamentoDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescontoComercial, "DescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ReservadoParaTextil0, "ReservadoParaTextil0")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ReservadoParaTextil1, "ReservadoParaTextil1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ReservadoParaTextil2, "ReservadoParaTextil2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ReservadoParaTextil3, "ReservadoParaTextil3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ReservadoParaTextil4, "ReservadoParaTextil4")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ReservadoParaTextil5, "ReservadoParaTextil5")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ReservadoParaTextil6, "ReservadoParaTextil6")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorBaseCalcRessarcSt, "ValorBaseCalcRessarcSt")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorIcmsRessarcSt, "ValorIcmsRessarcSt")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoDoProjeto, "CodigoDoProjeto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescontoDoProjeto, "DescontoDoProjeto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"CodRegistro":                      {0, 2, 0},
	"NumeroDoPedido":                      {2, 17, 0},
	"TipoCodProduto":                      {17, 20, 0},
	"CodEanProduto":                      {20, 34, 0},
	"UnidadesNaEmbalagem":                      {34, 40, 0},
	"DescricaoProduto":                      {40, 75, 0},
	"UnidadeMedida":                      {75, 78, 0},
	"QtdeSolicitada":                      {78, 87, 3},
	"ValorBrutoUnitario":                      {87, 104, 6},
	"PercentualBonificação":                      {104, 110, 4},
	"ValorIpi":                      {110, 123, 2},
	"ValorDespAcessoriasTributadas":                      {123, 136, 2},
	"ValorDespAcessoriasNaoTributadas":                      {136, 149, 2},
	"ValorIcmFonte":                      {149, 162, 2},
	"ValorFrete":                      {162, 175, 2},
	"CodigoReferencia":                      {175, 190, 0},
	"CodigoInternoPack":                      {190, 196, 0},
	"ValorDescontoNegociado":                      {196, 211, 7},
	"FormaPagamentoDesconto":                      {211, 213, 0},
	"DescontoComercial":                      {213, 219, 4},
	"ReservadoParaTextil0":                      {219, 226, 0},
	"ReservadoParaTextil1":                      {226, 237, 2},
	"ReservadoParaTextil2":                      {237, 242, 0},
	"ReservadoParaTextil3":                      {242, 262, 0},
	"ReservadoParaTextil4":                      {262, 275, 0},
	"ReservadoParaTextil5":                      {275, 285, 0},
	"ReservadoParaTextil6":                      {285, 300, 0},
	"ValorBaseCalcRessarcSt":                      {300, 315, 2},
	"ValorIcmsRessarcSt":                      {315, 330, 2},
	"CodigoDoProjeto":                      {330, 337, 0},
	"DescontoDoProjeto":                      {337, 341, 0},
	"Filler":                      {341, 350, 0},
}