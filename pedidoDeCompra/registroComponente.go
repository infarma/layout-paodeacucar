package pedidoDeCompra

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroComponente struct {
	CodRegistro        int64  	`json:"CodRegistro"`
	NumeroPedido       int64  	`json:"NumeroPedido"`
	CodEanProduto      int64  	`json:"CodEanProduto"`
	DescricaoCor       string 	`json:"DescricaoCor"`
	DescricaoTamanho   string 	`json:"DescricaoTamanho"`
	QtdeSolicitada     float64	`json:"QtdeSolicitada"`
	CodigoInternoPack  int64  	`json:"CodigoInternoPack"`
	QtdePackSolicitada float64	`json:"QtdePackSolicitada"`
	CodEanCorTamanho   int64  	`json:"CodEanCorTamanho"`
	Filler             string  	`json:"Filler"`
}

func (r *RegistroComponente) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroComponente

	err = posicaoParaValor.ReturnByType(&r.CodRegistro, "CodRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodEanProduto, "CodEanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescricaoCor, "DescricaoCor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescricaoTamanho, "DescricaoTamanho")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QtdeSolicitada, "QtdeSolicitada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoInternoPack, "CodigoInternoPack")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QtdePackSolicitada, "QtdePackSolicitada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodEanCorTamanho, "CodEanCorTamanho")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroComponente = map[string]gerador_layouts_posicoes.Posicao{
	"CodRegistro":                      {0, 2, 0},
	"NumeroPedido":                      {2, 17, 0},
	"CodEanProduto":                      {17, 31, 0},
	"DescricaoCor":                      {31, 51, 0},
	"DescricaoTamanho":                      {51, 71, 0},
	"QtdeSolicitada":                      {71, 80, 3},
	"CodigoInternoPack":                      {80, 86, 0},
	"QtdePackSolicitada":                      {86, 95, 3},
	"CodEanCorTamanho":                      {95, 108, 0},
	"Filler":                      {108, 350, 0},
}