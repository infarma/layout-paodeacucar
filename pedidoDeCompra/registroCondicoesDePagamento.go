package pedidoDeCompra

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroCondicoesDePagamento struct {
	CodRegistro                   int64  	`json:"CodRegistro"`
	NumeroPedido                  int64  	`json:"NumeroPedido"`
	TipoPagamento                 string 	`json:"TipoPagamento"`
	ReferênciaDePrazo            int64  	`json:"ReferênciaDePrazo"`
	DescricaoCondPagto            string 	`json:"DescricaoCondPagto"`
	QtdeDias                      int64  	`json:"QtdeDias"`
	DataDeVencimento              int64  	`json:"DataDeVencimento"`
	PorcentagemDescontoFinanceiro float64	`json:"PorcentagemDescontoFinanceiro"`
	PorcentagemAPagarFatura       float64	`json:"PorcentagemAPagarFatura"`
	Filler                        string 	`json:"Filler"`
}

func (r *RegistroCondicoesDePagamento) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroCondicoesDePagamento

	err = posicaoParaValor.ReturnByType(&r.CodRegistro, "CodRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TipoPagamento, "TipoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ReferênciaDePrazo, "ReferênciaDePrazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescricaoCondPagto, "DescricaoCondPagto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QtdeDias, "QtdeDias")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataDeVencimento, "DataDeVencimento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PorcentagemDescontoFinanceiro, "PorcentagemDescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PorcentagemAPagarFatura, "PorcentagemAPagarFatura")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroCondicoesDePagamento = map[string]gerador_layouts_posicoes.Posicao{
	"CodRegistro":                      {0, 2, 0},
	"NumeroPedido":                      {2, 17, 0},
	"TipoPagamento":                      {17, 20, 0},
	"ReferênciaDePrazo":                      {20, 21, 0},
	"DescricaoCondPagto":                      {21, 23, 0},
	"QtdeDias":                      {23, 26, 0},
	"DataDeVencimento":                      {26, 34, 0},
	"PorcentagemDescontoFinanceiro":                      {34, 40, 3},
	"PorcentagemAPagarFatura":                      {40, 45, 3},
	"Filler":                      {45, 350, 0},
}