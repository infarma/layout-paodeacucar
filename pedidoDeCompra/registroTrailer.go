package pedidoDeCompra

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RegistroTrailer struct {
	CodRegistro                           int64  	`json:"CodRegistro"`
	NumeroPedido                          int64  	`json:"NumeroPedido"`
	ValorTotalDespAcessoriasTributadas    float64	`json:"ValorTotalDespAcessoriasTributadas"`
	ValorTotalMercadorias                 float64	`json:"ValorTotalMercadorias"`
	ValorTotalBonificacao                 float64	`json:"ValorTotalBonificacao"`
	ValorTotalPedido                      float64	`json:"ValorTotalPedido"`
	ValorTotalDespAcessoriasNaoTributadas float64	`json:"ValorTotalDespAcessoriasNaoTributadas"`
	ValorTotalIcmFonte                    float64	`json:"ValorTotalIcmFonte"`
	ValorTotalFrete                       float64	`json:"ValorTotalFrete"`
	Filler                                string 	`json:"Filler"`
}

func (r *RegistroTrailer) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRegistroTrailer

	err = posicaoParaValor.ReturnByType(&r.CodRegistro, "CodRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalDespAcessoriasTributadas, "ValorTotalDespAcessoriasTributadas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalMercadorias, "ValorTotalMercadorias")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalBonificacao, "ValorTotalBonificacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalPedido, "ValorTotalPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalDespAcessoriasNaoTributadas, "ValorTotalDespAcessoriasNaoTributadas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalIcmFonte, "ValorTotalIcmFonte")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalFrete, "ValorTotalFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRegistroTrailer = map[string]gerador_layouts_posicoes.Posicao{
	"CodRegistro":                      {0, 2, 0},
	"NumeroPedido":                      {2, 17, 0},
	"ValorTotalDespAcessoriasTributadas":                      {17, 30, 2},
	"ValorTotalMercadorias":                      {30, 47, 6},
	"ValorTotalBonificacao":                      {47, 60, 2},
	"ValorTotalPedido":                      {60, 77, 6},
	"ValorTotalDespAcessoriasNaoTributadas":                      {77, 90, 2},
	"ValorTotalIcmFonte":                      {90, 103, 2},
	"ValorTotalFrete":                      {103, 116, 2},
	"Filler":                      {116, 350, 0},
}